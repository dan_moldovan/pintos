#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "../threads/palloc.h"

static void syscall_handler(struct intr_frame *);
static void colocvium1(void* esp);

void syscall_init(void) {
	intr_register_int(0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void syscall_handler(struct intr_frame *f UNUSED) {
	printf("system call!\n");
	int syscall_no = ((int*) f->esp)[0];
	switch (syscall_no) {
	case SYS_COLOCVIUM_1:
		char* threadName = (char*) (((int*) f->esp)[1]);
		int threadPriority = (((int*) f->esp)[2]);
		struct thread* newThread = thread_create(threadName, threadPriority,
				thread_function, (void*) 0);
		f->eax = newThread->thread_tick_count;
		break;
	case SYS_COLOCVIUM_2:
		int page_count = (((int*) f->esp)[1]);
		void* page_addr = palloc_get_multiple(PAL_USER & PAL_ZERO, page_count);
		f->eax = page_addr;

		struct allocs *newAlloc = (struct allocs*) malloc(
				sizeof(struct allocs));
		newAlloc->address = page_addr;
		newAlloc->page_count = page_count;
		list_push_back(thread_current()->user_allocs, newAlloc->elem);

		break;
	case SYS_COLOCVIUM_3:
		void* page_addr = (((int*) f->esp)[1]);
		int page_count;
		struct thread* t = thread_current();

		if (!list_empty(t->user_allocs)) {
			struct list_elem e;
			for (e = list_begin(&t->user_allocs);
					e != list_end(&t->user_allocs); e = list_next(e)) {
				struct allocs *a = list_entry(e, struct allocs, elem);
				if (a->address == page_addr) {
					page_count = a->page_count;
					f->eax = palloc_free_multiple(page_addr, page_count);
				}
			}
		}

		break;
	default:
		PANIC("Interrupt nr not recognized: %d\n", f->eax);
	}
	thread_exit();
}

