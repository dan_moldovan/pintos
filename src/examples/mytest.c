#include <stdio.h>
//#include <syscall.h>

#include "lib/user/syscall.h"
int main(int argc, char *argv[]) {
	create_simple_thread("t1", 1);
	create_simple_thread("t2", 2);
	create_simple_thread("t3", 3);

	char* addr = (char*) user_alloc(5 * sizeof(char));
	user_free(addr);
}
