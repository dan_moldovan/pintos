##a. Add a new field of type int in the thread structure named thread_tick_count. At thread initialization its value will be 0, then at every tick it will be incremented (+1) for the current thread (running thread).

###thread.h -> struct thread
```C
int thread_tick_count;
```

### thread.c -> init_thread()
```C
t->thread_tick_count = 0;
```

### thread.c -> thread_tick()
```C
t->thread_tick_count++l
```

##b. Create in the "thread.c" source file a function named thread_function with the following header:
void thread_function (void* aux);
The function will:
- obtain the current thread (running thread)
- then while thread_tick_count did not reached 25 it will write using printf the thread id, its name and the values of thread_tick_count and priority fields.

```C
void thread_function (void* aux) {
    struct thread* t = thread_current();
    if (t->thread_tick_count < 25) {
        printf("thread_function: id:%d, name:%s tick:%d priority:%d\n", t->tid, t->name, t->thread_tick_count, t->priority);
    }
}
```

##c. Implement a system call (syscall) with the following header:

int create_simple_thread(const char *name, int priority);
This syscall will create a new thread with the name and priority received as parameters. As function for the newly created thread you will use the function created at b. - thread_function; the sent parameter will be NULL.

### /lib/user/syscall.h -> define header of syscall
```C
int create_simple_thread(const char *name, int priority);
```

### /lib/user/syscall.c -> define implementation of syscall
### We use syscall2 because the function takes 2 arguments
```C
int
create_simple_thread(const char *name, int priority) {
    syscall2 (SYS_COLOCVIUM_1, name, priority);
    NOT_REACHED();
}
```

### /lib/syscall-nr.h -> add your syscall name there
```C
SYS_COLOCVIUM_1,
```

### /userprog/syscall.c -> get the syscall stack number and make a switch depending on it
```C
int syscall_no = ((int*) f->esp)[0];
switch (syscall_no) {
case SYS_COLOCVIUM_1:
    char* threadName = (char*) (((int*) f->esp)[1]);
    int threadPriority = (((int*) f->esp)[2]);
    struct thread* newThread = thread_create(threadName, threadPriority,
            thread_function, (void*) 0);
    f->eax = newThread->thread_tick_count;
    break;
```

##d. Create a new source file "mytest.c" in src/examples which will test the functionality implemented at a, b and c. The executable will create 3 threads "t1", "t2", "t3" having priorities 1, 2 and 3. You can run the test with: pintos -v --filesys-size=2 -p ../examples/mytest -a mytest.exe -- -f run 'mytest.exe'
15 min

### /examples/mytest.c -> Write the test
```C
#include <stdio.h>

#include "lib/user/syscall.h"
int main(int argc, char *argv[]) {
    create_simple_thread("t1", 1);
    create_simple_thread("t2", 2);
    create_simple_thread("t3", 3);
}

```

### /examples/Makefile -> add mytest.c to the Makefile with the code below + in the array at the start
```C
mytest_SRC = mytest.c
```

##e. Implement 2 syscalls: "void* user_alloc(int size);" and "void user_free(void *buffer);" which allocates and frees memory pages from user pool. Hint: use palloc_get_multiple and palloc_free_multiple from "threads/palloc.c". Allocated buffers will be saved (per thread) in a new list defined in the thread structure. (struct list user_allocs;) For the list's elements you will define a new structure named struct allocs with the following fields:
- a pointer (void*) to the allocated memory
- a field size_t containing the number of pages (allocated)
- a field of type struct list_elem (used for the list)
For list access you need to use a synchronization mechanism (read/write mutual exclusive access).


### thread.h -> define struct list user_allocs and save it as a field in struct thread
```C
struct allocs {
    void* address;
    size_t page_count;
    struct list_elem elem;
};
```
and
```C
struct list user_allocs;
```

### thread.c -> in thread_create() call list_init to initialize list of user_allocs
```C
list_init(t->user_allocs);
```

### /userprog/syscall.c -> include palloc.h and continue the switch statement by adding these 2 system calls
```C
#include "../threads/palloc.h"
```
and
```C
case SYS_COLOCVIUM_2:
    int page_count = (((int*) f->esp)[1]);
    void* page_addr = palloc_get_multiple(PAL_USER & PAL_ZERO, page_count);
    f->eax = page_addr;

    struct allocs *newAlloc = (struct allocs*) malloc(
            sizeof(struct allocs));
    newAlloc->address = page_addr;
    newAlloc->page_count = page_count;
    list_push_back(thread_current()->user_allocs, newAlloc->elem);

    break;
case SYS_COLOCVIUM_3:
    void* page_addr = (((int*) f->esp)[1]);
    int page_count;
    struct thread* t = thread_current();

    if (!list_empty(t->user_allocs)) {
        struct list_elem e;
        for (e = list_begin(&t->user_allocs);
                e != list_end(&t->user_allocs); e = list_next(e)) {
            struct allocs *a = list_entry(e, struct allocs, elem);
            if (a->address == page_addr) {
                page_count = a->page_count;
                f->eax = palloc_free_multiple(page_addr, page_count);
            }
        }
    }

    break;
```

### /lib/user/syscall.h -> headers for the 2 sysctem calls

```C
void* user_alloc(int size);
void user_free(void *buffer);
```

### /lib/user/syscall.c -> implementation for the 2 system calls
```C
void* user_alloc(int size){
    syscall1(SYS_COLOCVIUM_2, size);
    NOT_REACHED();
}
void user_free(void *buffer){
    syscall1(SYS_COLOCVIUM_3, buffer);
    NOT_REACHED();
}
```

##f. Call those 2 implemented syscalls (at point e) from the file "mytest.c". (Do not write anything in the allocated buffer with user_alloc.)

### /examples/mytest.c -> add allocation and free of memory to the test created before
```C
char* addr = (char*) user_alloc(5 * sizeof(char));
user_free(addr);
```
